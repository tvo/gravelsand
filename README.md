## Introduction

Gravel Sand is class lib which helps with some code generation for dotnet!

The core use-case for this project is to create a runtime class, which implements an interface, that forwards it's calls though a proxy class.

### Why?


##

code:

var proxy = new GravelSand.Proxy.DynamicProxy();

proxy.AddHandler("", p => "hello world");
var result = await proxy.Invoke<string>("", "", new List<Tuple<string, object>>());

