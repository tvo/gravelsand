using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using FluentAssertions;
using Xunit;
using GravelSand.Proxy;

namespace Tests
{
    public class DynamicProxyTests
    {
        [Fact]
        public async Task PassthroughWorldIsPassed()
        {
			// Given
			var proxy = new GravelSand.Proxy.DynamicProxy();
			var hit = false;
			proxy.AddHandler("", p => { hit = true; return "hello"; });

			// when
			var result = await proxy.Invoke<string>("", "", new List<Tuple<string, object>>());

			// Then
			result.Should().Be("world");
        }
    }

	public interface IHaveAMethod
	{
		string Method(string input);
	}
}
