namespace GravelSand.Serialization
{
	public interface ISerialize
	{
		T DeSerializeObject<T>(string content);
		string SerializeObject<T>(T ob);
	}
}
