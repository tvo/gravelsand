namespace GravelSand.Serialization
{
	public class JsonSerializer : ISerialize
	{
		public T DeSerializeObject<T>(string content) => Newtonsoft.Json.JsonConvert.DeserializeObject<T>(content);
		public string SerializeObject<T>(T obj) => Newtonsoft.Json.JsonConvert.SerializeObject(obj);
	}
}
