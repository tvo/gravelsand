using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GravelSand.Proxy
{
	using HandlerFuncType = System.Func<System.Collections.Generic.List<Tuple<string, object>>, object>;
	using HandlerAsyncFuncType = System.Func<System.Collections.Generic.List<Tuple<string, object>>, Task<object>>;

	public class DynamicProxy : IProxy
	{
		private Dictionary<string, HandlerAsyncFuncType> Outputs = new Dictionary<string, HandlerAsyncFuncType>();

		public void AddHandler(string methodName, HandlerFuncType handle)
		{
			Outputs.Add(methodName, ps => Task.FromResult(handle(ps)));
		}

		public void AddAsyncHandler(string methodName, HandlerAsyncFuncType handle)
		{
			Outputs.Add(methodName, handle);
		}

		public async Task<TR> Invoke<TR>(string hostType, string methodName, List<Tuple<string, object>> parameterList)
		{
			return (TR) await Outputs[methodName](parameterList);
		}
	}
}
