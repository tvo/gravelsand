using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GravelSand.Proxy
{
	public interface IProxy
	{
		Task<TR> Invoke<TR>(string hostType, string methodName, List<Tuple<string, object>> parameterList);
	}
}
