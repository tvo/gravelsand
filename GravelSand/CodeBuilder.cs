using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;

namespace GravelSand
{
	// our usings. Separate for clarity.
	using Proxy;
	using Serialization;

	public class CodeBuilder<T>
	{
		private readonly IProxy _proxy;
		private readonly ISerialize _serializer;

		const string fieldPrefix = "Callback__";
		private static readonly ImmutableList<MethodInfo> Methods = ImmutableList<MethodInfo>.Empty.AddRange(typeof(T).GetMethods().Where(x => x.IsPublic));

		public CodeBuilder(IProxy proxy, ISerialize serializer)
		{
			_proxy = proxy;
			_serializer = serializer;
		}

		public Task<TR> TheResult<TR>(string hostType, string methodName, List<Tuple<string, object>> parameterList)
		{
			return _proxy.Invoke<TR>(hostType, methodName, parameterList);
		}

		private void AddConstructor(TypeBuilder builder)
		{
			var constructor = builder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, new Type[] { });
			var ilGen = constructor.GetILGenerator();
			ilGen.Emit(OpCodes.Ret);
		}

		private void AddMethod(Type interfaceType, TypeBuilder builder, MethodInfo method, FieldBuilder fieldInfo)
		{
			var parameters = method.GetParameters().ToList();
			var parameterTypes = parameters.Select(x => x.ParameterType).ToArray();
			var methodBuilder = builder.DefineMethod(method.Name, MethodAttributes.Public | MethodAttributes.Virtual, method.ReturnType, parameterTypes);
			var ilGen = methodBuilder.GetILGenerator();

			// All the types/methods... Put here so that we don't call reflection calls in loops.
			var tupleTypes = new[] { typeof(string), typeof(object) };
			var tupleType = typeof(Tuple<,>).MakeGenericType(tupleTypes);
			var tupleCtor = tupleType.GetConstructor(tupleTypes);
			var listType = typeof(List<>).MakeGenericType(tupleType);
			var addMethod = listType.GetMethod("Add", new[] { tupleType });

			// create a list of parameters as a local variable
			var paramList = ilGen.DeclareLocal(listType);
			ilGen.Emit(OpCodes.Ldc_I4, parameters.Count);
			ilGen.Emit(OpCodes.Newobj, listType.GetConstructor(new[] { typeof(int) }));
			ilGen.Emit(OpCodes.Stloc, paramList); // var parameters = new List<Tuple<string, object>>(parameters.Count);

			for (var i = 0; i < parameters.Count; ++i)
			{
				// Create and set the local var, which is the parameter name and value
				var tup = ilGen.DeclareLocal(tupleType);
				ilGen.Emit(OpCodes.Ldstr, parameters[i].ParameterType.FullName);
				ilGen.Emit(OpCodes.Ldarg, i + 1);
				ilGen.Emit(OpCodes.Newobj, tupleCtor);
				ilGen.Emit(OpCodes.Stloc, tup);
				// add the tuple to the parameter list.
				ilGen.Emit(OpCodes.Ldloc, paramList);
				ilGen.Emit(OpCodes.Ldloc, tup);
				ilGen.Emit(OpCodes.Call, addMethod); // parameters.Add(new Tuple(argName, argValue));
			}

			// Load the func field for this method.
			ilGen.Emit(OpCodes.Ldarg_0);
			ilGen.Emit(OpCodes.Ldfld, fieldInfo);

			// call the generic method with name and full name arguments
			ilGen.Emit(OpCodes.Ldstr, interfaceType.Name);
			ilGen.Emit(OpCodes.Ldstr, method.Name);
			ilGen.Emit(OpCodes.Ldloc, paramList);
			ilGen.Emit(OpCodes.Callvirt, fieldInfo.FieldType.GetMethod("Invoke"));

			if (method.ReturnType.IsValueType && method.ReturnType != typeof(void))
				ilGen.Emit(OpCodes.Unbox_Any, method.ReturnType);

			if (method.ReturnType == typeof(void))
			{
				ilGen.Emit(OpCodes.Pop); // void doesn't have a return.
				ilGen.Emit(OpCodes.Ret); // return
			}
			else
				ilGen.Emit(OpCodes.Ret); // return name_callback<TResult>(name, fullName, arguments);

			builder.DefineMethodOverride(methodBuilder, method);
		}

		public T Create()
		{
			var interfaceType = typeof(T);
			var typeBuilder = GetTypeBuilder(interfaceType.Name.TrimStart('I') + "_ProxyGenerated"); // IExample => Example_ProxyGenerated
			typeBuilder.AddInterfaceImplementation(interfaceType);

			AddConstructor(typeBuilder);

			var fieldDictionary = AddCallbackFields(typeBuilder);
			foreach (var method in Methods)
				AddMethod(interfaceType, typeBuilder, method, fieldDictionary[method]);

			var resultType = typeBuilder.CreateType();
			var instance = (T)Activator.CreateInstance(resultType);
			SetCallbackFields(instance);

			// Implementation notes:
			// It's easier to add several callback methods, then set them individually rather than pass one callback in the constructor.
			// Could optimize later if needed.
			// Each of the methods call a func, because the func has a reference to this code builder class, which calls TheResult method.

			return instance;
		}

		private Dictionary<MethodInfo, FieldBuilder> AddCallbackFields(TypeBuilder builder)
		{
			var dictionary = new Dictionary<MethodInfo, FieldBuilder>();
			foreach (var method in Methods)
			{
				var fieldName = fieldPrefix + method.Name;
				var fieldType = typeof(Func<string, string, List<Tuple<string, object>>, object>);
				var field = builder.DefineField(fieldName, fieldType, FieldAttributes.Public | FieldAttributes.HasDefault);
				dictionary[method] = field;
			}
			return dictionary;
		}

		private void SetCallbackFields(T instance)
		{
			var instanceType = instance.GetType();
			foreach (var method in Methods)
			{
				Func<string, string, List<Tuple<string, object>>, object> callback;
				var fieldName = fieldPrefix + method.Name;

				var callbackType = method.ReturnType;

				var methodToInvoke = GetType().GetMethod(nameof(TheResult));
				// make the method non async
				if (callbackType != typeof(Task) && callbackType.BaseType != typeof(Task))
				{
					if (method.ReturnType == typeof(void))
						callbackType = typeof(object);

					methodToInvoke = methodToInvoke.MakeGenericMethod(callbackType);

					callback = (name, fullName, args) =>
					{
						var taskResult = methodToInvoke.Invoke(this, new object[] { name, fullName, args });
						var property = taskResult.GetType().GetProperty("Result");
						return property.GetValue(taskResult);
					};
				}
				else
				{
					// this is an async return;
					callbackType = method.ReturnType == typeof(Task) ? typeof(object) : method.ReturnType.GenericTypeArguments.First();
					methodToInvoke = methodToInvoke.MakeGenericMethod(callbackType);
					callback = (name, fullName, args) =>
					{
						return methodToInvoke.Invoke(this, new object[] { name, fullName, args });
					};
				}


				instanceType.GetField(fieldName).SetValue(instance, callback);
			}
		}

		private static TypeBuilder GetTypeBuilder(string className)
		{
			var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName(Guid.NewGuid().ToString()), AssemblyBuilderAccess.Run);
			var moduleBuilder = assemblyBuilder.DefineDynamicModule("MainModule");
			return moduleBuilder.DefineType(className,
					TypeAttributes.Public |
					TypeAttributes.Class |
					TypeAttributes.AutoClass |
					TypeAttributes.AnsiClass |
					TypeAttributes.BeforeFieldInit |
					TypeAttributes.AutoLayout,
					null);
		}
	}
}
